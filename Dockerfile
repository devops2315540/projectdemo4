FROM eclipse-temurin:17-jdk-jammy
WORKDIR /app
COPY target/*.jar /app
CMD ["java",-jar,"target/*.jar /app"]
